#!/bin/bash

set -e

function prepare_release () {
    echo "Preparing release"
    release next-version --allow-current > .next-version
    release changelog
    release commit-and-tag CHANGELOG.md release_info --create-tag-pipeline
    cat .next-version
}
