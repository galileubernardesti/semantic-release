FROM registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.1

# Metadata
LABEL maintainer="Fabio Coelho <fabiocruzcoelho@gmail.com>"

COPY scripts /scripts
