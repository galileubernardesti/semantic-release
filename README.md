# Sematic Release
É um simples conjunto de comandos que você pode usar para compor seu fluxo de trabalho de release automatizada no gitlab.

## Uso Minimo
- `.gitlab-ci.yml`

```yml
include:
  - project: estudosdevops/job/build/semantic-release
    ref: master
    file: src/gitlab-ci-release.yml
```

## Requerido
Criar arquivo `.releaserc.yml` na raiz do projeto

```yml
---
branch: master
noCi: true
analyzeCommits:
  preset: eslint
generateNotes:
  preset: eslint
release:
  verifyConditions:
    - "@semantic-release/changelog"
    - "@semantic-release/gitlab"
    - "@semantic-release/git"
  prepare:
    - make CLI_DIST_VERSION=${nextRelease.gitTag} dist
    - "@semantic-release/changelog"
    - "@semantic-release/gitlab"
  publish:
    path: "@semantic-release/gitlab"
    gitlabUrl: "https://gitlab.com"
  plugins:
    - "@semantic-release/commit-analyzer"
    - "@semantic-release/release-notes-generator"
    - "@semantic-release/changelog"
    - changelogFile: "docs/CHANGELOG.md"
    - "@semantic-release/git"
    - assets:
        - CHANGELOG.md
      message: >
        Chore: Release ${nextRelease.version}
        ${nextRelease.notes}
success: false
fail: false
npmPublish: false
tarballDir: false
tagFormat: "${version}"
```
## Formato dos commits
- `Formato:` type(scope): subject
    - `feat:` novo recurso para o usuário, não um novo recurso para o script de construção
    - `fix:` correção de bug para o usuário, não uma correção para um script de construção
    - `docs:` alterações na documentação
    - `style:` formatação, falta de ponto e vírgula, etc; nenhuma alteração no código de produção
    - `refactor:` refatoração do código de produção, por exemplo. renomeando uma variável
    - `test:` adicionando testes ausentes, refatorando testes; nenhuma alteração no código de produção
    - `chore:` atualização de tarefas grunhidas, etc; nenhuma alteração no código de produção

## Desenvolvimento inicial
A versão principal zero (0.y.z) é para desenvolvimento inicial. Qualquer coisa pode mudar a qualquer momento. A API pública não deve ser considerada estável.

## Como liberar a v1.0.0
Quando você estiver pronto para interromper o desenvolvimento inicial e começar a usar as regras normais da versão principal, adiciona a variável de ambiente no seu CI-CD `GSG_INITIAL_DEVELOPMENT:` para false. Na próxima versão, o sematic-release colidirá diretamente com a v1.0.0.

### Obs:
Se a versão principal não for 0, o valor de `GSG_INITIAL_DEVELOPMENT` será ignorado.
