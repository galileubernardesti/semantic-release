# CHANGELOG

<!--- next entry here -->

## 1.0.5
2020-03-03

### Fixes

- **added:** README.md (c58883e4fea54a3f6058af827fa73c3e598df2e1)

## 1.0.4
2020-03-03

### Fixes

- **remove:** Variables Lines (73f7e084361264d61c3deb2399b215e58d790a80)

## 1.0.3
2020-03-03

### Fixes

- **remove:** Variables (481b862682f737ac51cfb7a6e290c5dc66dc414e)

## 1.0.2
2020-02-06

### Fixes

- name function (31a5407b7b48c29e826e1104f98ccd87615680e1)

## 1.0.1
2020-02-04

### Fixes

- gitlab-ci (0958209254bc10f8824f9c221b1c80a5dd4466c9)
- **ci-cd:** gitlab-ci (c72845ba11deb517f54f72ad89cdb92150640bd9)
- **ci-cd:** gitlab-ci (450040e83d4f4c49fff4aff6c031b21c65b657f5)

## 1.0.0
2020-02-04

### Fixes

- **ci-cd:** gitlab-ci (a176b788a8cb13592d04e7c092b339de73c7113d)

## 0.2.2
2020-02-04

### Fixes

- **files:** Files refactors (acad6e0d3bfab580a2deef087301b80867e2efd6)
- **files:** Files refactors (57f37a852293ed1da318fbff1ced39e6a3f6a648)
- **files:** Files refactors (debf9fcdeb839a4fb4e826f6ddb8b64566663bbc)
- **files:** Files refactors (27a77d6dbd8282761e56d63fa3a508ef9027bab5)
- **files:** Files refactors (473cc6c9b1762e3edb9fa0ec1bf89fcb7f13e293)
- **ci-cd:** Files refactors (7b476788c53e776c0541b8c0007d731827429c0c)
- **ci-cd:** Files refactors (cb10f9cd7292a4d140bfc1728f0f69cf1305f34d)
- **ci-cd:** Files refactors (5b3dd47ef51d2c64bf8982d67da2bbfe2f5259b8)
- **ci-cd:** gitlab-ci-release (aba263b99e61089e767f70e803bc45f125ef464e)
- **ci-cd:** gitlab-ci-release (bffef42479ef6c3ecb19aaeb4a6a513d5f979beb)

## 0.2.1
2020-01-31

### Fixes

- **release:** Remove line duplicate (ef444363eaa39c6e98789c455d67c5141d951c71)

## 0.2.0
2020-01-31

### Features

- **functions:** Added script functions with command releases (03dd300087c10141e82dd4447e21dbc835af21aa)

### Fixes

- **releaserc:** Add intepractor (6ecaad1a02d2d9d6014046ac23c2e799afae66ac)
- **releaserc:** yaml (9c86e9b1229dd2d67c89dd021905a01234e8a9ca)